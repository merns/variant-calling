#/bin/bash

## Setup
CPU=20
JAVA_XMX=4g

PARALLEL_JOBS_PICARD=${CPU}
PARALLEL_LOG_PICARD=parallel_picard.log

## Uncomment for parallel dryrun
#PARALLEL_DRYRUN=--dryrun


if ! ls *.sorted.bam >/dev/null 2>&1; then 
	echo "Could not find any .sorted.bam files!"
	exit
fi


## Indexing BAM file
echo "Starting Picard AddOrReplaceReadGroups job..."
echo " - Parallal Jobs: ${PARALLEL_JOBS_PICARD}"
echo " - Max Memory per Job: ${JAVA_XMX}"
echo " - Read-Group ID: <filename>.sorted.bam"
echo " - Read-Group library: lib_Julia"
echo " - Read-Group platform: illumina"
echo " - Read-Group platform unit: unit1"
echo " - Read-Group sample name: <filename>.sorted.bam"
echo " - Input: *.sorted.bam"
echo " - Output: *.picardRG.bam"
echo ""
parallel ${PARALLEL_DRYRUN} \
    --jobs ${PARALLEL_JOBS_PICARD} \
    --joblog ${PARALLEL_LOG_PICARD} \
     picard -Xmx${JAVA_XMX} \
		AddOrReplaceReadGroups \
        I={} \
    	O='{= s:\.[^.]+$::;=}'.picardRG.bam \
    	RGID='{= s:\.[^.]+$::;s:\.[^.]+$::; =}' \
    	RGSM='{= s:\.[^.]+$::;s:\.[^.]+$::; =}' \
        RGLB=lib_Julia \
        RGPL=illumina \
    	RGPU=unit1 \
    ::: *.sorted.bam


echo "------------------------------------------------"
echo "Final log of parallel job runs"
echo "------------------------------------------------"
cat ${PARALLEL_LOG_PICARD}
