#/bin/bash

set -e

REF_GENOME=/data/006344F_arrow_Rev_PT1_PT2.fasta
JAVA_XMX=4g

CONFIG_MIN_READS_AT_LOCUS=3


## Checking preconditions of input files
if [ ! -f ${REF_GENOME} ]; then
    echo "Reference genome not found at: ${REF_GENOME}"
    exit
fi

if ! ls *.sorted.picardRG.bam >/dev/null 2>&1; then 
	echo "Could not find any .sorted.picardRG.bam files!"
	exit
fi


echo "Samtools: Merging all *.sorted.picardRG.bam files into merged.bam..."
echo "  Logging merged *.bam files in bamlist.txt"
if [ -f merged.bam ]; then 
	echo "  Output file merged.bam exists. Deleting."
	rm merged.bam
fi

ls *.sorted.picardRG.bam > bamlist.log
samtools merge -@ 10 -b bamlist.log merged.bam
samtools index merged.bam


## TODO: Fix output genome name to *.dict
if [ ! -f ${REF_GENOME}.dict ]; then 
	picard CreateSequenceDictionary \
		R=${REF_GENOME} \
		O=/data/006344F_arrow_Rev_PT1_PT2.dict
fi

samtools faidx ${REF_GENOME}


#Regions in need of realignment
# https://gatkforums.broadinstitute.org/gatk/discussion/11455/realignertargetcreator-and-indelrealigner
# https://gatkforums.broadinstitute.org/gatk/discussion/11455/realignertargetcreator-and-indelrealigner
# https://gatkforums.broadinstitute.org/gatk/discussion/3151/should-i-use-unifiedgenotyper-or-haplotypecaller-to-call-variants-on-my-data
# Comnparison UnifiedGenotyper vs. HaplotypeCaller vs. others: https://link.springer.com/article/10.1186/s12859-018-2147-9
gatk3 -Xmx${JAVA_XMX} \
    -T RealignerTargetCreator \
    -I merged.bam \
    -o merged_output.intervals \
    -R ${REF_GENOME} \
    --minReadsAtLocus 3


# Realigner over intervals
gatk3 -Xmx${JAVA_XMX} \
	-T IndelRealigner \
	-I merged.bam \
	-o merged_realigned.bam \
	-R ${REF_GENOME} \
	-targetIntervals merged_output.intervals \
	-LOD 3.0 \
	--maxReadsInMemory 1000000 \
	--maxReadsForRealignment 100000

## TODO: Invalid command line: The parameter standard_min_confidence_threshold_for_emitting is deprecated.  This argument is no longer used in GATK versions 3.7 and newer. Please see the online documentation for the latest usage recommendations.

# call SNPs with unified genotyper
gatk3 -Xmx${JAVA_XMX} \
    -T UnifiedGenotyper \
    -I merged_realigned.bam \
    -o rawSNPS_Q30.vcf \
	-R ${REF_GENOME} \
	-gt_mode DISCOVERY \
	-stand_call_conf 30 \
#	-stand_emit_conf 10

# Annotate Variants
gatk3 -Xmx${JAVA_XMX} \
    -T VariantAnnotator \
    -I merged_realigned.bam \
    -o rawSNPS_Q30_annotated.vcf \
	-R ${REF_GENOME} \
	-l INFO \
	-V:variant,VCF rawSNPS_Q30.vcf \
	--useAllAnnotations

# Calling Indels (needed for next step)
gatk3 -Xmx${JAVA_XMX} \
    -T UnifiedGenotyper \
    -I merged_realigned.bam \
    -o InDels_Q30.vcf \
	-R ${REF_GENOME} \
	-gt_mode DISCOVERY \
	-glm INDEL \
	-stand_call_conf 30 \
#	-stand_emit_conf 10

# Filtering around InDels:
gatk3 -Xmx${JAVA_XMX} \
    -T VariantFiltration\
    --mask InDels_Q30.vcf\
    -o Indel_filtered_Q30.vcf \
    --variant rawSNPS_Q30_annotated.vcf \
	-R ${REF_GENOME} \
    --maskName InDel  

# Additional Filtering
gatk3 -Xmx${JAVA_XMX} \
    -T VariantFiltration \
    --variant Indel_filtered_Q30.vcf \
    -o analysis_ready_Q30.vcf \
	-R ${REF_GENOME} \
	--clusterWindowSize 10 \
	--filterExpression "MQ0>=4 && ((MQ0 / (1.0*DP)) >0.1)" \
    --filterName "HARD_TO_VALIDATE" \
	--filterExpression "SB>= -1.0" \
    --filterName "StrandBiasFilter" \
	--filterExpression "QUAL < 10" \
    --filterName "QualFilter" \
	--filterExpression "QUAL < 30.0 || QD <5.0 || HRun >5 || SB > -0.10" \
    --filterName "GATKStandard"


# Finally only those which have Passed and have PASS
cat analysis_ready_Q30.vcf | grep 'PASS\|^#' > highQualSNPS.vcf


