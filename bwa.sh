#/bin/bash

## Setup
CPU=4
REF_GENOME=/data/data/CascadeHops_FALCON-Unzip_fc_quiver_primary.fasta

THREADS_BWA=2
PARALLEL_JOBS_BWA=$((${CPU} / ${THREADS_BWA}))
PARALLEL_LOG=parallel_bwa.log

## Uncomment for parallel dryrun
#PARALLEL_DRYRUN=--dryrun

## Checking preconditions of input files
if [ ! -f ${REF_GENOME} ]; then
    echo "Reference genome not found at: ${REF_GENOME}"
    exit
fi

if ! ls *.fastq >/dev/null 2>&1; then 
    echo "Could not find any .fastq files!"
    exit
fi


## Mapping
echo "Mapping using 'bwa mem'..."
echo " - Threads: ${THREADS_BWA}"
echo " - Parallal Jobs: ${PARALLEL_JOBS_BWA}"
echo " - Reference Genome: ${REF_GENOME}"
echo " - Input: *.fastq"
echo " - Output: *.sorted.bam"
echo ""

parallel ${PARALLEL_DRYRUN} \
    --jobs ${PARALLEL_JOBS_BWA} \
    --joblog ${PARALLEL_LOG} \
    bwa mem \
        -M \
        -t ${THREADS_BWA} \
        -V ${REF_GENOME} \
        '{= s:\.[^.]+$::;s:\.[^.]+$::; =}'.fastq \
    '|' samtools sort \
        -O bam \
        -T '{/.}' \
        -o '{/.}'.sorted.bam\
    ::: *.fastq

# for matching *fq.gz with parallel use: {= s:\.[^.]+$::;s:\.[^.]+$::; =}

echo "------------------------------------------------"
echo "Final log of parallel job runs"
echo "------------------------------------------------"
cat ${PARALLEL_LOG}








