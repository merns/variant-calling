# Improvements

Use Cromwell: https://cromwell.readthedocs.io/en/develop/tutorials/FiveMinuteIntro/


# Setup

## Docker Image Details

The docker image builds on Biodocker base image `biocontainers/biocontainers:v1.1.0_cv1`.

Adding the following packages via bioconda `conda` package manager:

* bwa=0.7.17
* gatk=3.8 / gatk=3.6/ gatk4=4.1.2.0
* samtools=1.9
* picard=2.20.0


## Clone the Git repository

```
git clone git@bitbucket.org:merns/variant-calling.git
```


## Build the Docker Image


1. Download Gatk 3.8 tar.bz2: XXX
2. Downlaod Gatk 3.6 tar.bz2: XXX


```sh
docker build -f Dockerfile.gatk36 -t marko/variant-calling:gatk36
docker build -f Dockerfile.gatk38 -t marko/variant-calling:gatk38  
docker build -f Dockerfile.gatk41 -t marko/variant-calling:gatk41
```



## Run the Docker Container


Make sure all data is in the same data directory.

```sh
DATADIR=$(pwd)/data
```


```sh
docker run -it --rm -v ${DATADIR}:/data marko/variant-calling:gatk36
```

### Optional: Newer Versions of Gatk

```sh
docker run -it --rm -v ${DATADIR}:/data marko/variant-calling:gatk38
docker run -it --rm -v ${DATADIR}:/data marko/variant-calling:gatk41
```


# Processing

## Indexing of the Genome

Create indices for the genome first:

```sh
GENOME_FILE=/data/CascadeHops_FALCON-Unzip_fc_quiver_primary.fasta
bwa index ${GENOME_FILE}
```


## Mapping with BWA MEM

... and sorting with samtools.


Run the script:

```sh
sh bwa.sh
```


## Picard Read-Group Annotation

Run the script:

```sh
sh picard.sh
```


## GatK Variant Calling (SNPs, InDels)

Run the script:

```sh
sh gatk3.sh
```


# Documentation

* BWA: 
	* Website: http://bio-bwa.sourceforge.net
	* Man Page: http://bio-bwa.sourceforge.net/bwa.shtml
* Picard: https://broadinstitute.github.io/picard/
	* AddOrReplaceGroups: https://broadinstitute.github.io/picard/command-line-overview.html#AddOrReplaceReadGroups
* GATK 3.6: https://software.broadinstitute.org/gatk/documentation/tooldocs/3.6-0/
* GATK 3.8: https://software.broadinstitute.org/gatk/documentation/tooldocs/3.8-0/
* Samtools: http://www.htslib.org/doc/samtools.html
* Parallel: 
	* https://www.gnu.org/software/parallel/parallel_cheat.pdf
	* https://www.gnu.org/software/parallel/parallel_tutorial.html#Perl-expression-replacement-string


# Citations

Check the Dockerfile for respective versions.

* __GATK__ DOI: http://dx.doi.org/10.1101/gr.107524.110
* __Picard__ Webpage: https://broadinstitute.github.io/picard/
* __BWA MEM__ DOI: https://doi.org/10.1093%2Fbioinformatics%2Fbtp324
* __Samtools__ DOI: https://doi.org/10.1093%2Fbioinformatics%2Fbtp352
* __Parallel__ DOI: http://dx.doi.org/10.5281/zenodo.16303

